#include "Header.h"

int main() {
	int input;
	printf("정수값을 입력하시면 입력된 값 \n크기의 배열을 생성하고 랜덤값으로 채웁니다.\n");
	printf("정수를 입력하세요:");
	fseek(stdin, 0, SEEK_END);
	scanf("%d", &input);

	int* arr = (int*)malloc(sizeof(int)*input);
	srand(time(0));
	for (int i = 0; i < input; i++) {
		arr[i] = rand() % 10 + 1;
	}

	printf("+++생성된 랜덤 배열 값+++\n");
	for (int i = 0; i < input; i++) {
		printf("[%d] ", arr[i]);
	}
	printf("\n");

	printf("+++각각의 배열 원소를 원의 반지름으로 봤을 때 의 원의 넓이+++\n");
	float* area = (float*) malloc(sizeof(float) * input);
	for (int i = 0; i < input; i++) {
		area[i] = arr[i] * arr[i] * calculate_pi();
	}
	for (int i = 0; i < input; i++) {
		printf("[%f]\n", area[i]);
	}

	fseek(stdin, 0, SEEK_END);
	fgetc(stdin);
	return 0;
}

float calculate_pi() {
	return 3.141592653589793238462643383279;
}